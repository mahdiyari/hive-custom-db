# Hive Custom DB

Start mysql docker from /Database-docker.

```
cd Database-docker
mkdir data

docker build --build-arg MYSQL_ROOT_PASSWORD=password -t hive-db-mysql .

docker run  -d -p 127.0.0.1:6603:3306 \
--mount type=bind,src=/root/hive-custom-db/Database-docker/config/my.cnf,dst=/etc/my.cnf \
--mount type=bind,src=/root/hive-custom-db/Database-docker/data,dst=/var/lib/mysql \
--name hive-mysql hive-db-mysql
```

Cereate config.js from config-example.js and edit it.
Start application from the root directory of project.

```
npm i
pm2 start src/index.js
```

Note: Running both app and mysql in docker is not easy and requires more extra work. The easiest way is using pm2. BTW dockerfile is there, if you can connect to mysql server from another container, goodluck.

---

### API

Post request:

- /api/voters  
  Req: { author: 'test', permlink: 'test'}  
  Res: { id: 1, voters: ['user1', 'user2']}

- /api/created  
  Req: { author: 'test', permlink: 'test'}  
  Res: { id: 1, created: '1970-01-01T00:00:00'}
