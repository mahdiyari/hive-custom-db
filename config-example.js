const config = {
  daysToIndex: 7,
  rpcNode: 'http://127.0.0.1:8091',
  dbName: 'hive-db',
  dbUser: 'root',
  dbPassword: 'password',
  dbHost: '127.0.0.1',
  dbPort: 6603
}

module.exports = config
