FROM node:12.18.4-alpine

RUN mkdir -p /home/node/hive-custom-db/node_modules && chown -R node:node /home/node/hive-custom-db

WORKDIR /home/node/hive-custom-db

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 6969

CMD [ "node", "app.js" ]