const con = require('./helpers/mysql')
const CronJob = require('cron').CronJob
const jobDay = new CronJob('0 0 * * *', async () => {
  try {
    const now = Date.now() / 1000
    const result = await con.query('SELECT `id` FROM `posts` WHERE `time`<?', [now - 604800])
    for (let i = 0; i < result.length; i++) {
      const id = result[i].id
      await con.query('DELETE FROM `votes` WHERE `post_id`=?', [id])
      await con.query('DELETE FROM `posts` WHERE `id`=?', [id])
    }
    const lastBlock = await con.query('SELECT `block_number` FROM `synced_blocks` ORDER BY `block_number` DESC LIMIT 1')
    const toRemove = lastBlock[0].block_number - 201600
    await con.query('DELETE FROM `synced_blocks` WHERE `block_number`<?', [toRemove])
  } catch (e) {
    throw new Error(e)
  }
}, () => {
  console.log('I think cron is done is done.')
},
true, /* Start the job right now */
'UTC' /* Time zone of this job. */
)

module.exports = jobDay
