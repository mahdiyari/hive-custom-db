const mysql = require('mysql')
const config = require('../../config')
const pool = mysql.createPool({
  connectionLimit: process.env.DB_LIMIT || 800,
  host: config.dbHost || '127.0.0.1',
  port: config.dbPort || 6603,
  user: config.dbUser || 'root',
  password: config.dbPassword || 'password',
  database: config.dbName || 'test',
  charset: 'utf8mb4'
})

// Rewriting MySQL query method as a promise
const con = {}
con.query = async (query, val) => {
  if (val) {
    const qu = await new Promise((resolve, reject) => {
      pool.query(query, val, (error, results) => {
        if (error) reject(new Error(error))
        resolve(results)
      })
    })
    return qu
  } else {
    const qu = await new Promise((resolve, reject) => {
      pool.query(query, (error, results) => {
        if (error) reject(new Error(error))
        resolve(results)
      })
    })
    return qu
  }
}

module.exports = con
