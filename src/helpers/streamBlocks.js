const call = require('./nodeCall')
const config = require('../../config')

const INTERVAL_TIME = 1800 // 1s

const streamBlockNumber = async (cb) => {
  try {
    let lastBlock = 0
    setInterval(async () => {
      const result = await call(
        config.rpcNode,
        'condenser_api.get_dynamic_global_properties',
        []
      )
      if (
        result &&
        result.head_block_number &&
        !isNaN(result.head_block_number)
      ) {
        if (result.head_block_number > lastBlock) {
          lastBlock = result.head_block_number
          cb(lastBlock)
        }
      }
    }, INTERVAL_TIME)
  } catch (e) {
    if (
      e &&
      e.message &&
      e.message.indexOf('Unable to acquire database lock') > -1
    ) {
      //
    } else {
      console.error('At stream blocks: ', e)
    }
  }
}

const streamBlockOperations = async (cb) => {
  try {
    streamBlockNumber(async (blockNumber) => {
      const { operations, timestamp } = await getOperations(blockNumber)
      if (operations.length > 0) {
        for (const operation of operations) {
          cb(operation, blockNumber, timestamp)
        }
      }
    })
  } catch (e) {
    if (
      e &&
      e.message &&
      e.message.indexOf('Unable to acquire database lock') > -1
    ) {
      //
    } else {
      console.error('At stream ops: ', e)
    }
  }
}

const streamOpsInBlock = async (cb) => {
  try {
    streamBlockNumber(async (blockNumber) => {
      const operations = await getOpsInBlock(blockNumber)
      if (operations.length > 0) {
        for (const ops of operations) {
          cb(ops.op, blockNumber, ops.txId)
        }
      }
    })
  } catch (e) {
    if (
      e &&
      e.message &&
      e.message.indexOf('Unable to acquire database lock') > -1
    ) {
      //
    } else {
      console.error('At stream ops: ', e)
    }
  }
}

const getOpsInBlock = async (blockNumber) => {
  const result = await call(config.rpcNode, 'condenser_api.get_ops_in_block', [
    blockNumber,
    false
  ])
  if (result) {
    const operations = result.map((transaction) => {
      return { op: transaction.op, txId: transaction.trx_id }
    })
    return operations
  }
  return []
}

const getOperations = async (blockNumber) => {
  const result = await call(config.rpcNode, 'condenser_api.get_block', [
    blockNumber
  ])
  if (result) {
    const operations = result.transactions.map((transaction) => {
      return transaction.operations
    })
    const timestamp = result.timestamp
    return { operations, timestamp }
  }
}

module.exports = {
  streamBlockNumber,
  streamBlockOperations,
  getOperations,
  streamOpsInBlock,
  getOpsInBlock
}
