const express = require('express')
const router = express.Router()
const con = require('../helpers/mysql')

router.post('/voters', async (req, res) => {
  if (!req || !req.body) {
    res.sendStatus(400)
    return
  }
  const author = req.body.author
  const permlink = req.body.permlink
  if (!author || !permlink || author.length > 16 || permlink.length > 256) {
    res.sendStatus(400)
    return
  }
  const respond = {
    id: 1,
    voters: []
  }
  const postId = await con.query(
    'SELECT `id` FROM `posts` WHERE `author`=? AND `permlink`=?',
    [author, permlink]
  )
  if (!postId || !Array.isArray(postId) || postId.length < 1) {
    res.json(respond)
    return
  }
  const id = postId[0].id
  const votesDB = await con.query(
    'SELECT `voter` FROM `votes` WHERE `post_id`=?',
    [id]
  )
  if (!votesDB || !Array.isArray(votesDB) || votesDB.length < 1) {
    res.json(respond)
    return
  }
  for (let i = 0; i < votesDB.length; i++) {
    respond.voters.push(votesDB[i].voter)
  }
  res.json(respond)
})

module.exports = router
