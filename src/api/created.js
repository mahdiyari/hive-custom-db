const express = require('express')
const router = express.Router()
const con = require('../helpers/mysql')

router.post('/created', async (req, res) => {
  if (!req || !req.body) {
    res.sendStatus(400)
    return
  }
  const author = req.body.author
  const permlink = req.body.permlink
  if (!author || !permlink || author.length > 16 || permlink.length > 256) {
    res.sendStatus(400)
    return
  }
  const respond = {
    id: 1,
    created: '1970-01-01T00:00:00',
    is_comment: true
  }
  const creation = await con.query(
    'SELECT `created`, `parent_author` FROM `posts` WHERE `author`=? AND `permlink`=?',
    [author, permlink]
  )
  if (!creation || !Array.isArray(creation) || creation.length < 1) {
    res.json(respond)
    return
  }
  respond.created = creation[0].created
  if (creation[0].parent_author && creation[0].parent_author.length > 0) {
    res.json(respond)
    return
  }
  respond.is_comment = false
  res.json(respond)
})

module.exports = router
