const express = require('express')
const app = express()
const voters = require('./voters')
const created = require('./created')

app.use('/', voters)
app.use('/', created)

module.exports = app
