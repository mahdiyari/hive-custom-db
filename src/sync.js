const con = require('./helpers/mysql')
const config = require('../config')
const call = require('./helpers/nodeCall')
const stream = require('./helpers/streamBlocks')
const rapidQueue = require('./helpers/queue')

const queue = rapidQueue.createQueue()

const DAYS_TO_INDEX = config.daysToIndex

let totalSyncedBlocks = 0
let lastSyncedBlock = 0

let firstRun = true
// let lastBlock = 0
const syncBlocks = () => {
  stream.streamBlockNumber((blockNum) => {
    if (firstRun) {
      queueOldBlocks(blockNum)
      firstRun = false
    }
    queue.push(blockNum)
    // if (lastBlock === 0) {
    //   lastBlock = blockNum
    // }
    // if (blockNum > lastBlock) {
    //   lastBlock = blockNum
    // }
  })
}

const queueOldBlocks = (nowBlock) => {
  const blocksToCatch = (DAYS_TO_INDEX * 24 * 60 * 60) / 3
  const oldestBlock = nowBlock - blocksToCatch
  for (let i = oldestBlock; i < nowBlock; i++) {
    queue.push(i)
  }
}

const intervalTime = 10 // 50ms
const maxI = 150
let queueIndex = 0
const processQueue = () => {
  setInterval(() => {
    const L = queue.length()
    if (queueIndex < maxI && L > 0) {
      const n = maxI - queueIndex > L ? L : maxI - queueIndex
      for (let k = 0; k < n; k++) {
        const blockNum = queue.shift()
        processBlock(blockNum)
      }
    }
  }, intervalTime)
}
processQueue()

const processBlock = async (blockNum) => {
  queueIndex++
  try {
    const alreadySynced = await con.query(
      'SELECT `id` FROM `synced_blocks` WHERE `block_number`=?',
      [blockNum]
    )
    if (Array.isArray(alreadySynced) && alreadySynced.length > 0) {
      queueIndex--
      return
    }
    const { operations, timestamp } = await stream.getOperations(blockNum)
    if (operations.length > 0) {
      for (const ops of operations) {
        await processOps(ops, timestamp)
      }
    }
    await updateSyncedBlocks(blockNum)
    totalSyncedBlocks++
  } catch (e) {}
  queueIndex--
}

// let syncingProcess = 0
// const getOlderBlocks = async (nowBlock) => {
//   const blocksToCatch = 403200 // 14days/3s
//   const oldestBlock = nowBlock - blocksToCatch
//   const startTime = Date.now() / 1000
//   console.log(`Start syncing from ${oldestBlock} to ${nowBlock}`)
//   const int = setInterval(() => console.log(syncingProcess + '%'), 30000)
//   for (let i = oldestBlock; i < nowBlock; i++) {
//     const alreadySynced = await con.query(
//       'SELECT `id` FROM `synced_blocks` WHERE `block_number`=?',
//       [i]
//     )
//     if (Array.isArray(alreadySynced) && alreadySynced.length > 0) {
//       continue
//     }
//     const { operations, timestamp } = await stream.getOperations(i)
//     if (operations.length > 0) {
//       for (const ops of operations) {
//         processOps(ops, timestamp)
//       }
//     }
//     updateSyncedBlocks(i)
//     syncingProcess = Math.floor(((i - oldestBlock) / blocksToCatch) * 100)
//   }
//   const endTime = Date.now() / 1000
//   console.log(
//     '###### Finished syncing past 14 days in ' +
//       (endTime - startTime) / 60 +
//       'minutes.'
//   )
//   clearInterval(int)
// }

// setInterval(() => {
//   console.log(
//     'Total synced blocks: ' +
//       totalSyncedBlocks +
//       ' - Last synced block: ' +
//       lastSyncedBlock
//   )
// }, 30000)

const processOps = async (ops, timestamp) => {
  for (const op of ops) {
    if (op[0] === 'comment') {
      await processComment(op, timestamp)
    } else if (op[0] === 'vote') {
      await processVote(op)
    }
  }
}

const processComment = async (op, timestamp, retry = 0) => {
  if (
    !op ||
    !op[1] ||
    // allow comments too
    // op[1].parent_author !== '' ||
    !op[1].author ||
    !op[1].permlink ||
    !timestamp
  ) {
    return
  }
  const postExists = await con.query(
    'SELECT `id` FROM `posts` WHERE `author`=? AND `permlink`=?',
    [op[1].author, op[1].permlink]
  )
  if (Array.isArray(postExists) && postExists.length > 0) {
    return
  }

  const findComments = await call(
    config.rpcNode,
    'database_api.find_comments',
    { comments: [[op[1].author, op[1].permlink]] }
  )
  if (!findComments || !findComments.comments || !findComments.comments[0]) {
    if (retry === 0) {
      console.log('retrying for ' + op[1].author + '/' + op[1].permlink)
      setTimeout(() => {
        processComment(op, timestamp, 1)
      }, 3000)
    }
    return
  }
  const created = findComments.comments[0].created
  const createdTime = Math.floor(new Date(created + 'Z').getTime() / 1000)
  const now = Date.now() / 1000
  if (now - createdTime > 604800) {
    return
  }

  const parentPermlink = op[1].parent_permlink || ''
  const parentAuthor = op[1].parent_author || ''
  const metadata = safeParseJSON(op[1].json_metadata) || ''
  const tagsArray =
    (metadata && Array.isArray(metadata.tags) && metadata.tags) || []
  const tags = JSON.stringify(tagsArray)
  await con.query(
    'INSERT INTO `posts`(`author`, `permlink`, `created`, `tags`, `parent_permlink`, `parent_author`, `time`) ' +
      'SELECT ?, ?, ?, ?, ?, ?, ? FROM DUAL ' +
      'WHERE NOT EXISTS (SELECT `id` FROM `posts` WHERE `author`=? AND `permlink`=? LIMIT 1)',
    [
      op[1].author,
      op[1].permlink,
      created,
      tags,
      parentPermlink,
      parentAuthor,
      createdTime,
      op[1].author,
      op[1].permlink
    ]
  )
}

const processVote = async (op) => {
  if (
    !op ||
    !op[1] ||
    !op[1].voter ||
    !op[1].author ||
    !op[1].permlink ||
    !op[1].weight
  ) {
    return
  }
  const postExists = await con.query(
    'SELECT `id` FROM `posts` WHERE `author`=? AND `permlink`=?',
    [op[1].author, op[1].permlink]
  )
  if (!Array.isArray(postExists) || postExists.length < 1) {
    return
  }
  const postId = postExists[0] && postExists[0].id
  const voteExists = await con.query(
    'SELECT `id` FROM `votes` WHERE `post_id`=? AND `voter`=?',
    [postId, op[1].voter]
  )
  if (Array.isArray(voteExists) && voteExists.length > 0) {
    return
  }
  await con.query(
    'INSERT INTO `votes`(`post_id`, `voter`, `weight`) VALUES(?,?,?)',
    [postId, op[1].voter, op[1].weight]
  )
}

const updateSyncedBlocks = async (blockNum) => {
  await con.query('INSERT INTO `synced_blocks`(`block_number`) VALUES(?)', [
    blockNum
  ])
  lastSyncedBlock = blockNum
}

const safeParseJSON = (json) => {
  try {
    return JSON.parse(json)
  } catch (e) {
    return null
  }
}

module.exports = syncBlocks

// curl -s --data '{"jsonrpc":"2.0", "method":"database_api.find_comments", "params": {"comments": [["emrebeyler", "auto-trainer-for-rabona"]]}, "id":1}' http://127.0.0.1:8091
// curl -s --data '{"jsonrpc":"2.0", "method":"database_api.list_comments", "params": {"start":["masummim50", "portrait-painting-in-clip-studio-paint"], "limit":10, "order":"by_permlink"}, "id":1}' https://api.openhive.network
// curl -s --data '{"jsonrpc":"2.0", "method":"account_history_api.get_account_history", "params":{"account":"masummim50", "start":-1, "limit":5}, "id":1}' https://api.openhive.network
// curl -s --data '{"jsonrpc":"2.0", "method":"condenser_api.get_content", "params":["masummim50", "portrait-painting-in-clip-studio-paint"], "id":1}' https://api.openhive.network
// curl -s --data '{"jsonrpc":"2.0", "method":"condenser_api.get_ops_in_block", "params":[47000000, false], "id":1}' http://127.0.0.1:8091
// CREATE TABLE `test`.`posts` ( `id` BIGINT NOT NULL AUTO_INCREMENT ,  `author` TEXT NOT NULL ,  `permlink` TEXT NOT NULL ,  `created` BIGINT NOT NULL ,  `tags` TEXT NULL DEFAULT NULL ,  `parent_permlink` TEXT NULL DEFAULT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;
// ALTER TABLE `posts` ADD FULLTEXT(`permlink`);
// ALTER TABLE `posts` ADD FULLTEXT(`author`);
// ALTER TABLE `posts` ADD INDEX(`created`);
