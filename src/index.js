const express = require('express')
const bodyParser = require('body-parser')
const sync = require('./sync')
const cronjob = require('./cronjob')
// const cookieParser = require('cookie-parser')
const hpp = require('hpp')
const helmet = require('helmet')
const app = express()
const api = require('./api')

app.use(hpp())
app.use(helmet())

// support json encoded bodies and encoded bodies
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
// app.use(cookieParser())

app.use(function (req, res, next) {
  /** TODO: add ENV var for development mode */
  // const dev = 0
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Credentials', true)
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, access_key'
  )
  next()
})
app.use('/api', api)

const port = process.env.PORT || 6009
const host = process.env.HOST || '0.0.0.0'
app.listen(port, host, () => {
  console.log(`Application started on ${host}:${port}`)
  if (process.env.NODE_APP_INSTANCE === '0') {
    sync()
    cronjob.start()
    console.log('sync started on process 0')
  }
})
